package it.unisalento.pas.smartcitywastemanagement.dto;

public class TaxDTO {



    private String id;
    private String citizenId;
    private int year;
    private double amount;
    private String status;

    public TaxDTO(String citizenId, int year, double amount, String status) {
        this.citizenId = citizenId;
        this.year = year;
        this.amount = amount;
        this.status = status;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCitizenId() {
        return citizenId;
    }

    public void setCitizenId(String citizenId) {
        this.citizenId = citizenId;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
