package it.unisalento.pas.smartcitywastemanagement.repositories;

import it.unisalento.pas.smartcitywastemanagement.domain.Tax;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;
import java.util.Optional;


public interface TaxRepository extends MongoRepository<Tax, String> {
    Optional<Tax> findByCitizenIdAndYear(String citizenId, Integer year);
    List<Tax> findByCitizenId(String citizenId);
}