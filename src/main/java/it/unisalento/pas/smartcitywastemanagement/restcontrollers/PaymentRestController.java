package it.unisalento.pas.smartcitywastemanagement.restcontrollers;

import it.unisalento.pas.smartcitywastemanagement.domain.Payment;
import it.unisalento.pas.smartcitywastemanagement.domain.Tax;
import it.unisalento.pas.smartcitywastemanagement.dto.PaymentDTO;
import it.unisalento.pas.smartcitywastemanagement.repositories.PaymentRepository;
import it.unisalento.pas.smartcitywastemanagement.repositories.TaxRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.Optional;

@CrossOrigin
@RestController
@RequestMapping("/api/payments")
public class PaymentRestController {

    @Autowired
    private PaymentRepository paymentRepository;

    @Autowired
    private TaxRepository taxRepository;


    @PreAuthorize("hasAnyRole('ADMIN','CITIZEN')")
    @PostMapping("/makePayment")
    public ResponseEntity<?> makePayment(@RequestBody PaymentDTO paymentDTO) {
        Locale locale = new Locale("it", "IT"); // Imposta la localizzazione italiana
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss", locale); // Formato data-ora italiano
        String italianDateTime = dateFormat.format(new Date()); // Ottieni la data e l'ora attuali formattate in italiano

// Imposta la data e l'ora attuali nel pagamento

        // Cerca la tassa corrispondente all'ID fornito
        Optional<Tax> taxOptional = taxRepository.findById(paymentDTO.getTaxId());
        if (!taxOptional.isPresent()) {
            return ResponseEntity.badRequest().body("Tassa non trovata con l'ID fornito.");
        }

        Tax tax = taxOptional.get();
        // Controlla se l'importo pagato corrisponde all'importo dovuto della tassa
        if (tax.getAmount() != paymentDTO.getAmountPaid()) {
            return ResponseEntity.badRequest().body("L'importo pagato non corrisponde all'importo dovuto della tassa.");
        }

        // Crea e salva il pagamento
        Payment payment = new Payment();
        payment.setTaxId(paymentDTO.getTaxId());
        payment.setAmountPaid(paymentDTO.getAmountPaid());
        payment.setPaymentMethod(paymentDTO.getPaymentMethod());
        payment.setPaymentDate(italianDateTime); // Imposta la data e l'ora attuali
        Payment savedPayment = paymentRepository.save(payment);

        // Aggiorna lo stato della tassa a "pagato"
        tax.setStatus("pagato");
        taxRepository.save(tax);

        return ResponseEntity.ok(savedPayment);
    }

    @PreAuthorize("hasAnyRole('ADMIN','CITIZEN','COMUNE')")
    @GetMapping("/byTaxId/{taxId}")
    public ResponseEntity<PaymentDTO> getPaymentByIdTax(@PathVariable String taxId) {
        return paymentRepository.findByTaxId(taxId)
                .map(payment -> convertToDTO(payment)) // Usa il metodo convertToDTO per convertire Payment in PaymentDTO
                .map(ResponseEntity::ok) // Crea una ResponseEntity con il PaymentDTO
                .orElse(ResponseEntity.notFound().build()); // Se non viene trovato nessun pagamento, restituisce 404 Not Found
    }

    private PaymentDTO convertToDTO(Payment payment) {
        PaymentDTO paymentDTO = new PaymentDTO();
        paymentDTO.setId(payment.getId());
        paymentDTO.setTaxId(payment.getTaxId());
        paymentDTO.setAmountPaid(payment.getAmountPaid());
        paymentDTO.setPaymentMethod(payment.getPaymentMethod());
        paymentDTO.setPaymentDate(payment.getPaymentDate());
        return paymentDTO;
    }

}
