package it.unisalento.pas.smartcitywastemanagement.restcontrollers;

import it.unisalento.pas.smartcitywastemanagement.domain.Tax;
import it.unisalento.pas.smartcitywastemanagement.dto.AuthenticationResponseDTO;
import it.unisalento.pas.smartcitywastemanagement.dto.LoginDTO;
import it.unisalento.pas.smartcitywastemanagement.dto.PerformanceDTO;
import it.unisalento.pas.smartcitywastemanagement.dto.TaxDTO;
import it.unisalento.pas.smartcitywastemanagement.repositories.TaxRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.*;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import static it.unisalento.pas.smartcitywastemanagement.security.Scheduling.password;
import static it.unisalento.pas.smartcitywastemanagement.security.Scheduling.username;

@RestController
@RequestMapping("/api/taxes")
@CrossOrigin
public class TaxRestController {

    @Autowired
    private RestTemplate restTemplate;

    @Autowired
    private TaxRepository taxRepository;

    @Scheduled(cron = "0 0 0 1 1 ?") // 0 secondi, 0 minuti  ,0 ora, mese, giorno quindi a mezzanotte di gennaio 1
    public void calculateTaxesAutomatically() {
        int previousYear = LocalDate.now().minusYears(1).getYear();


        // Prepara il body della richiesta per l'autenticazione
        LoginDTO loginDTO = new LoginDTO();
        loginDTO.setUsername(username); // Sostituisci con l'username effettivo
        loginDTO.setPassword(password); // Sostituisci con la password effettiva

        //  String authUrl = "http://my-sba-login:8080/api/users/authenticate";
        String authUrl = "https://dfaxe8nxs4.execute-api.us-east-1.amazonaws.com/dev/users/authenticate";

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<LoginDTO> entity = new HttpEntity<>(loginDTO, headers);

        try {
            // Effettua la chiamata POST per ottenere il token
            ResponseEntity<AuthenticationResponseDTO> response = restTemplate.postForEntity(authUrl, entity, AuthenticationResponseDTO.class);

            if (response.getStatusCode().equals(HttpStatus.OK) && response.getBody() != null) {
                String authToken = response.getBody().getJwt();
                System.out.println("Token JWT ottenuto per lo scheduling: " + authToken);


                calculateTaxesInternal(previousYear, authToken);
            } else {
                System.err.println("Errore durante l'autenticazione. Status: " + response.getStatusCode());
            }
        } catch (Exception e) {
            System.err.println("Errore durante la chiamata all'API di autenticazione: " + e.getMessage());
        }
    }

    @PreAuthorize("hasAnyRole('COMUNE', 'ADMIN')")
    @PostMapping("/calculateTaxes")
    public ResponseEntity<List<Tax>> calculateTaxes(@RequestHeader("Authorization") String authToken, @RequestBody Map<String, Object> requestBody) {
        if (!(requestBody.get("year") instanceof Integer year)) {
            return ResponseEntity.badRequest().build();
        }

        List<Tax> taxes = calculateTaxesInternal(year, authToken);
        return ResponseEntity.ok(taxes); // Restituisce direttamente la lista di oggetti Tax
    }

    private List<Tax> calculateTaxesInternal(Integer year, String authToken) {
        List<PerformanceDTO> performances = getPerformances(year, authToken);
        List<Tax> updatedTaxes = new ArrayList<>();

        final double tassaBase = 120.0;
        final double costanteUtilizzo = 5.0;

        for (PerformanceDTO performance : performances) {
            double tassaUtilizzo = performance.getPerformanceAnnuale().stream()
                    .filter(p -> p.getAnno() == year)
                    .flatMap(p -> p.getPerformanceMensili().stream())
                    .mapToDouble(m -> m.getIndicePerformance() * costanteUtilizzo / 100)
                    .sum();

            double tassaTotale = tassaBase + tassaUtilizzo;

            // Cerca se esiste già una tassa per l'anno e l'ID cittadino specificati
            Optional<Tax> existingTaxOpt = taxRepository.findByCitizenIdAndYear(performance.getIdCittadino(), year);
            Tax tax;
            if (existingTaxOpt.isPresent()) {
                // Se esiste, aggiorna il valore della tassa
                tax = existingTaxOpt.get();
                tax.setAmount(tassaTotale);
            } else {
                // Altrimenti, crea un nuovo record
                tax = new Tax();
                tax.setCitizenId(performance.getIdCittadino());
                tax.setYear(year);
                tax.setAmount(tassaTotale);
                tax.setStatus("non pagato");
            }
            updatedTaxes.add(taxRepository.save(tax));
        }

        return updatedTaxes;
    }
    private List<PerformanceDTO> getPerformances(Integer year, String authToken) {
        //String url = "http://my-sba-performance:8080/api/performances/getPerformancesByYear?anno=" + year;
        String url = "https://tyr4s160tg.execute-api.us-east-1.amazonaws.com/dev/performances/getPerformancesByYear?anno=" + year;
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);


        String token = authToken.startsWith("Bearer ") ? authToken.substring(7) : authToken;
        headers.setBearerAuth(token); // Usa il token "pulito" senza "Bearer "

        HttpEntity<?> entity = new HttpEntity<>(headers);

        ResponseEntity<List<PerformanceDTO>> response = restTemplate.exchange(
                url,
                HttpMethod.GET,
                entity,
                new ParameterizedTypeReference<List<PerformanceDTO>>() {}
        );

        return response.getBody();
    }

    @GetMapping("/byCitizen/{citizenId}")
    public ResponseEntity<List<Tax>> getTaxesByCitizen(@PathVariable String citizenId) {
        List<Tax> taxes = taxRepository.findByCitizenId(citizenId);
        if (taxes.isEmpty()) {
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok(taxes);
    }
}
